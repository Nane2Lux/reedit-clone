from fastapi import FastAPI, Body, Response, status, HTTPException
from pydantic import BaseModel, validator
from typing import List, Optional
from random import randrange


class BlogPost(BaseModel):
    title: str
    content: str
    published: bool = True #default value for the field
    rating: Optional[int] = None #fully Optional
    highlight: Optional[str] = "1" or None #fully optional
    
app = FastAPI()

#Path operator or Route

#decorator
data = [
            {"id":1, "title": "Welcome to our blog", 
             "content":"This is the begining", 
             "author": "Madhuri Poornima"
            }, 
            {"id":2, "title": "Top 10 best activities in Luxembourg",
             "content":"Our list of...", 
             "author": "Dishab Helene"
            }
        ]

#find and return the indes of a specific post
def find_post(given_id):
    for post in data:
        if post["id"]== given_id:   
            return post

@app.get("/posts") # READ
def get_post(): 
    return {f"{data}"} # response


def find_post_index(given_id):
    for index, post in enumerate(data):
            if post["id"] == given_id:
                return post


@app.post("/createPosts") # CREATE
def create_post(new_post: BlogPost, response: Response):
    post_dict = new_post.dict()
    post_dict["id"] = 3
    return {"data": post_dict} # response

@app.get("/posts/{id_param}") # READ
def read_post(id_param: int):
    wanted_post = find_post(id_param)
    if not wanted_post:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f"No correponding post was found with id:{id_param}")
    return {"data": wanted_post} # response # response

@app.put("/posts/{id_param}") # UPDATE
def put_post(id_param: int, new_post: BlogPost = Body):
    if not find_post([id_param]):
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                        detail = f"No correponding post was found with id:{id_param}")
    post_dict = new_post.dict()
    post_dict["id"] = id_param
    data[id_param] = post_dict
    return post_dict # response

#@app.patch("/posts/:{id_param}") # UPDATE
#def patch_post():
#    return f"{data}" # response

@app.delete('/posts/{id_param}') # DELETE
def del_post(id_param: int):
    if not find_post(id_param):
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                        detail = f"No correponding post was found with id:{id_param}")
    data.pop(id_param - 1)
    
    print (data) # response
    return Response(status_code = status.HTTP_204_NO_CONTENT)
