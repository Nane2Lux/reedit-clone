import psycopg2
from psycopg2.extras import RealDictCursor # add fields to cursor
from random import randrange
from fastapi import Body, FastAPI, HTTPException, Response, status
from pydantic import BaseModel  # Schema
from typing import Optional  # Optional fields

try:
    db_connection = psycopg2.connect(
        host ='localhost',
        database = 'onetomany',
        user = 'postgres',
        password = '240896',
        cursor_factory = RealDictCursor 
    )
    cursor = db_connection.cursor()
    print('DB connction : successful')
    
except Exception as error:
    print('DB conneciton : failed')
    print('Error', error)

class Book(BaseModel):  # Pydantic schema for POST Body validation
    title: str
    published_date: str
    published_house_id: int
    
class Pubhouse(BaseModel):  # Pydantic schema for POST Body validation
    name: str
    category: str
    
###########################################################
##########          FastAPI instance name         #########
###########################################################

app = FastAPI()  

###########################################################
####          GET /books ==> GET ALL THE BOOKS          ###
###########################################################
@app.get("/books")
def get_book():
    # Exectuting the SQL query
    cursor.execute("SELECT * FROM book")
    
    #Retrive all the posts (list)
    db_book = cursor.fetchall()
    return {"data": db_book}

###########################################################
####   GET /houses ===> GET ALL THE PUBLISHED HOUSES    ###
###########################################################

@app.get("/houses")
def get_phouse():
    # Exectuting the SQL query
    cursor.execute("SELECT * FROM published_house")
    
    #Retrive all the posts (list)
    db_phouse = cursor.fetchall()
    return {"data": db_phouse}


###########################################################
####          POST /books ===> CREATE NEW BOOK          ###
###########################################################

@app.post("/book", status_code=status.HTTP_201_CREATED)   # POST /book endpoint
def create_book(new_book: Book, response: Response):  # Use the pydantic schema class
    try:
        # verifiy if the house already exist in the table PUBLISHED_HOUSE
        cursor.execute("INSERT INTO book (title, published_house_id) VALUES (%s,%s) RETURNING *;", 
                        (new_book.title, new_book.published_house_id))
        
        book_dict = cursor.fetchone()
        db_connection.commit(); #save the changes to the DB
    
        return {"data": book_dict}
    except psycopg2.errors.ForeignKeyViolation as err:
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Foreign key violation with writer_id: {new_book.published_house_id}")


###########################################################
####    POST /house ===> CREATE NEW PUBLISHING HOUSE    ###
###########################################################

@app.post("/house", status_code=status.HTTP_201_CREATED)   # POST /house endpoint
def create_house(new_phouse: Pubhouse, response: Response):  # Use the pydantic schema class
    try:
        
        cursor.execute("INSERT INTO published_house (name, category) VALUES (%s,%s) RETURNING *;", 
                        (new_phouse.name, new_phouse.category))
        
        house_dict = cursor.fetchone()
        db_connection.commit(); #save the changes to the DB
    
        return {"data": house_dict}
    
    except psycopg2.errors.ForeignKeyViolation as err:
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Foreign key violation with writer_id: {new_phouse.published_house_id}")


###########################################################
####  DELETE /books/{id_param} ===> DELETE BOOK WITH ID ###
###########################################################

@app.delete('/books/{id_param}')
def delete_book(id_param: int):
    
    # Remove element from array
    cursor.execute('DELETE FROM book WHERE id = %s RETURNING * ;', (id_param,))
    book_dict = cursor.fetchone()
    
    db_connection.commit();
    
    # If No corresponding post found throw an exception to the consumer
    if not book_dict:                             
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    
    # Best practice HTTP CODE 204 = not content response
    return Response(status_code=status.HTTP_204_NO_CONTENT)  


###########################################################
#### DELETE /houses/{id_param}  ***DELETE HOUSE WITH ID ###
###########################################################

@app.delete('/houses/{id_param}')
def delete_house(id_param: int):
    
    # Remove element from array
    cursor.execute('DELETE FROM published_house WHERE id = %s RETURNING * ;', (id_param,))
    house_dict = cursor.fetchone()
    
    db_connection.commit();
    # If No corresponding post found throw an exception to the consumer
    if not house_dict:                             
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    
    # Best practice HTTP CODE 204 = not content response 
    return Response(status_code=status.HTTP_204_NO_CONTENT) 

###########################################################
####   PUT /book/{id_param} ===> REPLACE BOOK WITH ID   ###
###########################################################

@app.put('/book/{id_param}')
def replace_book(id_param: int, updated_book: Book):
    try:
        # Exception if not found
        cursor.execute('UPDATE book SET title = %s, published_house_id = %s WHERE id = %s RETURNING *;',
                    (updated_book.title, updated_book.published_house_id, id_param))
        
        book_dict = cursor.fetchone()
        db_connection.commit()
     
        if not book_dict:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Not corresponding book was found with id:{id_param}"
            )
        return book_dict
    
    except psycopg2.errors.UndefinedTable as err:
            raise HTTPException(status_code = status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail = f"Modification of an unexistant item from BOOK: {id_param}")

###########################################################
####  PUT /house/{id_param} ===> REPLACE HOUSE WITH ID  ###
###########################################################
            
#PUT /house/{id_param}  ***REPLACE POST WITH ID***
@app.put('/house/{id_param}')
def replace_house(id_param: int, updated_house: Pubhouse):
    try:
        # Exception if not found
        cursor.execute('UPDATE Pubhouse SET name = %s, category = %s WHERE id = %s RETURNING *;',
                    (updated_house.name, updated_house.category, id_param))
        
        house_dict = cursor.fetchone()
        db_connection.commit()
     
        if not house_dict:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Not corresponding House was found with id:{id_param}"
            )
        return house_dict
    
    except psycopg2.errors.UndefinedTable as err:
            raise HTTPException(status_code = status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail = f"Modification of an unexistant item from PUBLISHED_HOUSE: {id_param}")
            