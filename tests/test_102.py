import pytest

def total_with_tip(bill, percentage):
    if (bill < 0 or percentage < 0):
        raise Exception("Bill and percentage have to be positive")
    return bill + bill*percentage/100

@pytest.mark.parametrize("num1, num2 , expectation", [
    (10, 20, 12),
    (100, 20, 120),
    #(0, 0, 5),
    (0, 0, 0)
])

def test_tip_bulk(num1, num2, expectation):
    assert total_with_tip(num1, num2) == expectation
   