from sqlite3 import Timestamp
from jose import jwt
from v7_auth.utilities.jwt_manager import SERVER_KEY, ALGOTYHTM
import pytest

# testing the post creation
@pytest.mark.parametrize("title, content, writer_id, status_code", [
    ('First time to test', 'blabla',1, 200),
    #('First time to test', 'blabla',2, 422),
    ('First time to test', 'blabla', 2, 422)
])
def test_create_post(authorized_client, title, content, writer_id, status_code):
    res = authorized_client.post("/post", 
                     data ={"title":title, "content":content, "writer_id" : writer_id})
    # ASC
    
    assert res.status_code == status_code
    
# testing the get post
@pytest.mark.parametrize("post_id, status_code", [
    (1, 404),
    (2, 404)
])
def test_post(post_id, authorized_client, status_code):
    id = str(post_id)
    res = authorized_client.get('/post/' + id)
    assert res.status_code == status_code
    
def test_all_post(client):
    res = client.get('/posts')
    assert res.status_code == 401
    
@pytest.mark.parametrize("post_id, title, content, published, writer_id, status_code", [
    (15,"whatever", "whoever reads this", True, 2, 404), 
    (1,"whatever", "whoever reads this", None, None, 422)
    ])

def update_one_post(authorized_client, post_id, title, content, published, writer_id, status_code):
    id = str(post_id)
    res = authorized_client.put("/post/"+id, json={"title":title, "content": content, "published":published, "writer_id":writer_id})
    assert res.status_code == status_code

@pytest.mark.parametrize("post_id, status_code", [(4, 404), (2, 404)])
def test_get_delete_one(post_id, authorized_client, status_code):
    id = str(post_id)
    res = authorized_client.delete('/post/'+ id)
    assert res.status_code == status_code
    

    