import pytest
from fastapi.testclient import TestClient
from v7_auth.main import app
from v7_auth.models import Base
from v7_auth.database import get_db
from tests.test_user import test_me

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

DATABASE_URL = 'postgresql://postgres:240896@localhost:5432/sqlalchemy_test'

database_engine = create_engine(DATABASE_URL)

TestingSessionTemplate = sessionmaker(autocommit = False,
                              autoflush = False, bind=database_engine)

def override_get_db():
    db = TestingSessionTemplate()
    try:
        yield db
    finally:
        db.close()

@pytest.fixture()  
def session():
    # clean DB by deleting prévious tables
    Base.metadata.drop_all(bind=database_engine)
    #create tables
    Base.metadata.create_all(bind=database_engine)
    app.dependency_overrides[get_db] = override_get_db
    
@pytest.fixture()
def create_user(client):
    user_credentials = {"email":"test.user1@domain.lu","password":"1234"}
    #post request to create a new user
    res = client.post("/users", json = user_credentials)
    # response json including ONLY id, email and created_at
    new_user = res.json()
    # adding password to response
    new_user['password'] = user_credentials['password']
    # return new user including id, email, created_at and plain text password
    return new_user

@pytest.fixture
def user_token(create_user, client):
    res = client.post("/auth/", data = {"username": create_user['email'], "password": create_user['password']})
    return res.json().get("access_token")

@pytest.fixture
def authorized_client(client, user_token):
    client.headers = {**client.headers, "Authorization": f"Bearer {user_token}"}
    return client
    
@pytest.fixture()
def create_blog(client):
    post_credentials = {"title":"First time to test",
                        "content":"blabla",
                        "writer_id": 1}
                        #"writer_id": test_me(create_user, authorized_client)}
    res = client.post("/post/", json = post_credentials)
    new_post = res.json()
    new_post['writer_id'] = post_credentials['writer_id']
    return new_post

@pytest.fixture()
def client(session):
    yield TestClient(app)

