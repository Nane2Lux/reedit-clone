#recieve and handle the HTTP request

# ajout par André mais ça fait quoi?
from operator import mod
from statistics import mode

from typing import List
from .database import get_db, database_engine
from fastapi import Depends, FastAPI, HTTPException, Response, status
from sqlalchemy.orm import Session
from . import models, schemas
import sqlalchemy
from .utilities import hash_manager, jwt_manager
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from fastapi.middleware.cors import CORSMiddleware



###########################################################
####      create the tables if they don't exist yet     ###
###########################################################

models.Base.metadata.create_all(bind = database_engine)

###########################################################
#####    FastAPI instance name / Run the server   #########
###########################################################
app = FastAPI() 

###########################################################
#####                  CORS setUp                 #########
###########################################################
app.add_middleware(
    CORSMiddleware,
    allow_origins = ["*"], 
    allow_credentials =  True,
    allow_methods = ["*"],
    allow_headers = ["*"]
)

###########################################################
####     GET /blogposts ===> GET ALL THE Blogposts      ###
####         Fetch all data from BlogPost table         ###
###########################################################

@app.get('/posts', response_model = List[schemas.BlogPost_Response])
def get_posts(db: Session = Depends(get_db), user_id = Depends(jwt_manager.decode_token)):
    all_posts = db.query(models.BlogPost).all()
    return all_posts


###########################################################
####      POST /Blogpost ===> CREATE NEW BLOGPSOT       ###
####            Create a new Blogpost row               ###
###########################################################

@app.post('/post', response_model = schemas.BlogPost_Response)
def create_post(post_body: schemas.BlogpostPy, db:Session = Depends(get_db), user_id = Depends(jwt_manager.decode_token)):
    new_post = models.BlogPost(**post_body.dict())
    db.add(new_post)
    db.commit()
    db.refresh(new_post)
    return new_post


###########################################################
####       GET /blogpost ==> GET Blogpost with ID       ###
####           select me as an existing User            ###
###########################################################

@app.get('/post/{uri_id}', response_model = schemas.User_Response)
def get_post_sec(uri_id: int, db: Session = Depends(get_db), user_id: int = Depends(jwt_manager.decode_token)):     
    wanted_blogpost = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id).first()
    if not wanted_blogpost:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f"No corresponding User found in the DB:{user_id}")
    return wanted_blogpost

###########################################################
####   DELETE /blogpost{id} ==> DELETE one blogpost     ###
####          delete an existing Blogspot row           ###
###########################################################

@app.delete('/post/{uri_id}')
def delete_post(uri_id: int, db: Session = Depends(get_db), user_id: int = Depends(jwt_manager.decode_token)):
    del_post = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id)
    if not del_post.first():
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND, 
                            detail = f"No corresponding post found in the DB:{uri_id}")
    del_post.delete()
    db.commit()
    return Response(status_code=status.HTTP_204_NO_CONTENT)
    
#################################################################
####  PUT /blogpost/{id_param} ===> REPLACE BLOGPOST WITH ID  ###
#################################################################
        
@app.put('/post/{uri_id}',response_model = schemas.BlogPost_Response)
def replace_post(uri_id: int, 
                 post_body: schemas.BlogpostPy, 
                 user_id: int = Depends(jwt_manager.decode_token),
                 db: Session = Depends(get_db)):
    # Exception if not found    
    up_blog = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id)
    up_blogpost = up_blog.first()
    
    if not up_blogpost:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f"No corresponding Post found in the DB:{uri_id}")
    
    up_blog.update(post_body.dict())
    db.commit()
    return up_blogpost            

################## USERS ENDPOINTS ########################
###########################################################
####              GET all the users endpoint            ###
###########################################################
@app.get('/users', response_model=List[schemas.User_Response])
def get_users(db: Session = Depends(get_db), user_id: int = Depends(jwt_manager.decode_token)):
    all_users = db.query(models.User).all()
    return all_users


###########################################################
####             GET /me_user ==> GET my user           ###
####           select me as an existing User            ###
###########################################################

@app.get('/users/me', response_model = schemas.User_Response)
def get_user_me(user_id: int = Depends(jwt_manager.decode_token), db: Session = Depends(get_db)):     
    wanted_user = db.query(models.User).filter(models.User.id == user_id).first()
    if not wanted_user:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f"No corresponding User found in the DB:{user_id}")
    return wanted_user

###########################################################
####           GET /user{id} ==> GET one user           ###
####            select an existing User row             ###
###########################################################

@app.get('/users/{uri_id}', response_model = schemas.User_Response)
def get_user(uri_id: int, db: Session = Depends(get_db), user_id = Depends(jwt_manager.decode_token)):
    wanted_user = db.query(models.User).filter(models.User.id == uri_id).first()
    if not wanted_user:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f"No corresponding User found in the DB:{uri_id}")
    return wanted_user

###########################################################
####          POST /user ===> CREATE NEW USER           ###
####               Create a new User row                ###
###########################################################

@app.post('/users', status_code = status.HTTP_201_CREATED, response_model = schemas.User_Response)
def create_user(user_body: schemas.UserPy, db:Session = Depends(get_db)):
    
    # catch the pwd enter by the user and hash it
    pwd_hashed = hash_manager.hash_pass(user_body.password)
    
    # overwrite the plain pwd in DB by the hashed one
    user_body.password = pwd_hashed
    
    new_user = models.User(**user_body.dict())
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user

###########################################################
####                POST /auth end point                ###
####                                                    ###
###########################################################
incorrectException = HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED, 
            detail = "Incorrect username or password",
            headers = {"WWW-Authenticate":"Bearer"}
        )

@app.post('/auth', status_code = status.HTTP_202_ACCEPTED, response_model =  schemas.Token)
def auth_user(user_credentials: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    # get corresponding user from DB / defining corresponding_user as a models.User 
    # -> give it the structure of the data you catch
    corresponding_user: models.User = db.query(models.User).filter(models.User.email == user_credentials.username).first()
    
    # if not corresponding user found, raise an exception
    if not corresponding_user:
        raise incorrectException
    
    pass_valid = hash_manager.verify_password(user_credentials.password, corresponding_user.password)
    
    # if password invalid, raise an exception
    if not pass_valid:
        raise incorrectException
    
    jwt = jwt_manager.generate_token(corresponding_user.id)
    return jwt



###########################################################
####        DELETE /user{id} ==> DELETE one user        ###
####            delete an existing User row             ###
###########################################################

@app.delete('/users/{uri_id}')
def delete_user(uri_id: int = Depends(jwt_manager.decode_token), db: Session = Depends(get_db)):
    try:
        deleted_user = db.query(models.User).filter(models.User.id == uri_id).first()
        if not deleted_user:
            raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f"No corresponding User found in the DB:{uri_id}")
        db.delete(deleted_user)
        db.commit()
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    
    except sqlalchemy.exc.InvalidRequestError as err:
            raise HTTPException(status_code = status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail = f"Delete of an unexistant item from BOOK: {uri_id}")
    
###########################################################
####   PUT /user/{id_param} ===> REPLACE USER WITH ID   ###
###########################################################

#PUT /user/{id_param}  ***REPLACE USER WITH ID***
@app.put('/users/{id_param}',response_model = schemas.User_Response)
def replace_user(uri_id: int , user_body: schemas.UserPy, up_usr = Depends(jwt_manager.decode_token), db:Session = Depends(get_db)):
    # Exception if not found
    up_usr = db.query(models.User).filter(models.User.id == uri_id)
    up_user = up_usr.first()
    
    if not up_user:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f"No corresponding User found in the DB:{uri_id}")
    
    up_usr.update(user_body.dict(), synchronize_session = False)
    db.commit()
    
    return up_user
 