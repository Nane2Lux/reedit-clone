import psycopg2
from psycopg2.extras import RealDictCursor # add fields to cursor
from random import randrange
from fastapi import Body, FastAPI, HTTPException, Response, status
from pydantic import BaseModel  # Schema
from typing import Optional  # Optional fields

try:
    db_connection = psycopg2.connect(
        host ='localhost',
        database = 'Reedit Clone',
        user = 'postgres',
        password = '240896',
        cursor_factory = RealDictCursor 
    )
    cursor = db_connection.cursor()
    print('DB connction : successful')
    
except Exception as error:
    print('DB conneciton : failed')
    print('Error', error)

class BlogPost(BaseModel):  # Pydantic schema for POST Body validation
    title: str
    content: str
    published: bool = True
    rating: Optional[int] = None
    
###### FastAPI instance name ######
app = FastAPI()  

#######################################
#### "Path Operations" or "Routes"  ###
#######################################

# GET / :
@app.get("/")  # decorator
def hello():  # function
    return {"message": "Hello from my first API"}  # response

# GET /posts ***GET ALL BLOGPOSTS***
@app.get("/posts")
def get_posts():
    # Exectuting the SQL query
    cursor.execute("SELECT * FROM posts")
    
    #Retrive all the posts (list)
    db_posts = cursor.fetchall()
    
    return {"data": db_posts}

# POST /posts  ***CREATE NEW BLOGPOST***
@app.post("/posts", status_code=status.HTTP_201_CREATED)   # POST /posts endpoint
def create_posts(new_post: BlogPost, response: Response):  # Use the pydantic schema class
    cursor.execute("INSERT INTO posts (title, content, published) VALUES (%s,%s,%s) RETURNING *;", 
                       (new_post.title, new_post.content, new_post.published))
    
    post_dict = cursor.fetchone()
    db_connection.commit(); #save the changes to the DB
    
    # response.status_code = 201                           # Alternative solution to change the HTTP code
    return {"data": post_dict}    

#GET /posts/trending  ***GET LATEST BLOGPOST***
"""@app.get('/posts/trending')                                # Path order matters
def trending_posts():
    return {"data": my_posts[len(my_posts)-1]}"""

#GET /posts/{id_param}  ***GET POST WITH ID***
@app.get('/posts/{id_param}')                              # {id_param}  is the path parameter
def get_post(id_param: int, response: Response):           # id_param must be an integer                                       
    cursor.execute('SELECT * FROM posts WHERE id = %s;', (id_param,)) 

    post_dict = cursor.fetchone()
    
    if not post_dict:                             # If No corresponding post found throw an exception to the consumer
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    return {"data": post_dict}   

#DELETE /posts/{id_param}  ***DELETE POST WITH ID***
@app.delete('/posts/{id_param}')
def delete_post(id_param: int):
    
    # Remove element from array
    # my_posts.pop(corresponding_index)
    cursor.execute('DELETE FROM posts WHERE id = %s RETURNING * ;', (id_param,))
    post_dict = cursor.fetchone()
    
    db_connection.commit();
    
    if not post_dict:                             # If No corresponding post found throw an exception to the consumer
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    
    return Response(status_code=status.HTTP_204_NO_CONTENT) # Best practice HTTP CODE 204 = not content response 

#PUT /posts/{id_param}  ***REPLACE POST WITH ID***
@app.put('/posts/{id_param}')
def replace_post(id_param: int, updated_post: BlogPost):
    # Exception if not found
    cursor.execute('UPDATE posts SET title = %s, content = %s, published = %s WHERE id = %s RETURNING *;',
                   (updated_post.title, updated_post.content, updated_post.published, id_param))
    
    post_dict = cursor.fetchone()
    db_connection.commit()
     
    if not post_dict:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
        
    return post_dict
