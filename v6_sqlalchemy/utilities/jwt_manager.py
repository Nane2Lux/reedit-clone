from fastapi import HTTPException, status, Depends
from fastapi.security import OAuth2PasswordBearer
from jose import jwt, JWTError
from ..schemas import Token


SERVER_KEY = "71facbdaa6e29aba6eeb50c67a8b25eb1b61c81891cf6a95d0b4fe4f9d476e8f"
ALGORITHM = "HS256"

oauth2_scheme = OAuth2PasswordBearer(tokenUrl = "/auth")

###########################################################
####                  Encode function                   ###
###########################################################

def generate_token(user_id: int): # -> Token
    payload = {"user_id": user_id}
    encode_jwt = jwt.encode(payload, SERVER_KEY,  algorithm = ALGORITHM)
    return Token(access_token = encode_jwt, token_type = "bearer")

###########################################################
####                  Decode function                   ###
###########################################################

def decode_token(provided_token: str = Depends(oauth2_scheme) ): # -> str or -> Error
    try:
        payload = jwt.decode(provided_token, SERVER_KEY, algorithms = [ALGORITHM])
        decoded_id: str = payload.get("user_id")
    except JWTError:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED, 
            detail = "Could not validate credentials",
            headers = {"WWW-Authenticate":"Bearer"}
        )
    return decoded_id