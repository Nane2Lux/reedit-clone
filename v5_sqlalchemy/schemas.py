# pydantic schemas to exchange data structures between API server and client consumer

from datetime import datetime
from pydantic import BaseModel, EmailStr

###########################################################
####                      UserPy                        ###
####      Pydantic schema for User Body validation      ###
###########################################################

class UserPy(BaseModel):
    email: EmailStr
    password: str
    
###########################################################
####         Pydantic schema for User Response          ###
###########################################################

class User_Response(BaseModel):
    id: int
    email: EmailStr
    created_at: datetime
    #important for pydantic model/schema translation
    class Config: 
        orm_mode = True
    
###########################################################
####                    BlogpostPy                      ###
####      Pydantic schema for Post Body validation      ###
###########################################################

class BlogpostPy(BaseModel):
    title: str
    content: str
    writer_id: int

###########################################################
####           Pydantic schema Post Response            ###
###########################################################

class BlogPost_Response(BlogpostPy):
    id: int
    published: bool
    created_at: datetime
    writer: User_Response
    #important for pydantic model/schema translation
    class Config: 
        orm_mode = True