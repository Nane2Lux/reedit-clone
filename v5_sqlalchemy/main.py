#recieve and handle the HTTP request

from typing import List
from .database import get_db, database_engine
from fastapi import Depends, FastAPI, HTTPException, Response, status
from sqlalchemy.orm import Session
from . import models, schemas
import sqlalchemy

###########################################################
####      create the tables if they don't exist yet     ###
###########################################################

models.Base.metadata.create_all(bind = database_engine)

###########################################################
#####    FastAPI instance name / Run the server   #########
###########################################################
app = FastAPI() 

###########################################################
####          GET /users ==> GET ALL THE users          ###
####           Fetch all data from User table           ###
###########################################################

@app.get('/users', response_model = List[schemas.User_Response])
def get_users(db: Session = Depends(get_db)):
    all_users = db.query(models.User).all()
    return all_users

###########################################################
####     GET /blogposts ===> GET ALL THE Blogposts       ###
####         Fetch all data from BlogPost table         ###
###########################################################

@app.get('/blogposts', response_model = List[schemas.BlogPost_Response])
def get_blogposts(db: Session = Depends(get_db)):
    all_blogposts = db.query(models.BlogPost).all()
    return all_blogposts

###########################################################
####           GET /user{id} ==> GET one user           ###
####            select an existing User row             ###
###########################################################

@app.get('/user/{uri_id}', response_model = schemas.User_Response)
def get_user(uri_id: int, db: Session = Depends(get_db)):
    wanted_user = db.query(models.User).filter(models.User.id == uri_id).first()
    if not wanted_user:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f"No corresponding User found in the DB:{uri_id}")
    return wanted_user

###########################################################
####       GET /blogpost{id} ==> GET one blogpost       ###
####          select an existing Blogspot row           ###
###########################################################

@app.get('/blogpost/{uri_id}', response_model = schemas.BlogPost_Response)
def get_blogposts(uri_id: int, db: Session = Depends(get_db)):
    wanted_blogpost = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id).first()
    if not wanted_blogpost:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f"No corresponding Post found in the DB:{uri_id}")
    return wanted_blogpost
    
    
###########################################################
####          POST /user ===> CREATE NEW USER           ###
####               Create a new User row                ###
###########################################################

@app.post('/user', response_model = schemas.User_Response)
def create_user(user_body: schemas.UserPy, db:Session = Depends(get_db)):
    new_user = models.User(**user_body.dict())
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user

###########################################################
####      POST /Blogpost ===> CREATE NEW BLOGPSOT       ###
####            Create a new Blogpost row               ###
###########################################################

@app.post('/blogpost', response_model = schemas.BlogPost_Response)
def create_blogpost(blogpost_body: schemas.BlogpostPy, db:Session = Depends(get_db)):
    new_blogpost = models.BlogPost(**blogpost_body.dict())
    db.add(new_blogpost)
    db.commit()
    db.refresh(new_blogpost)
    return new_blogpost

###########################################################
####        DELETE /user{id} ==> DELETE one user        ###
####            delete an existing User row             ###
###########################################################

@app.delete('/user/{uri_id}')
def delete_user(uri_id: int, db: Session = Depends(get_db)):
    try:
        deleted_user = db.query(models.User).filter(models.User.id == uri_id).first()
        if not deleted_user:
            raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f"No corresponding User found in the DB:{uri_id}")
        db.delete(deleted_user)
        db.commit()
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    
    except sqlalchemy.exc.InvalidRequestError as err:
            raise HTTPException(status_code = status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail = f"Delete of an unexistant item from BOOK: {uri_id}")
    

###########################################################
####   DELETE /blogpost{id} ==> DELETE one blogpost     ###
####          delete an existing Blogspot row           ###
###########################################################

@app.delete('/blogpost/{uri_id}')
def delete_blogpost(uri_id: int, db: Session = Depends(get_db)):
    try:
        del_blogpost = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id).first()
        if not del_blogpost:
            raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f"No corresponding blogpost found in the DB:{uri_id}")
        db.delete(del_blogpost)
        db.commit()
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    except sqlalchemy.exc.InvalidRequestError as err:
            raise HTTPException(status_code = status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail = f"Delete of an unexistant item from BOOK: {uri_id}")
            
###########################################################
####   PUT /user/{id_param} ===> REPLACE USER WITH ID   ###
###########################################################

#PUT /user/{id_param}  ***REPLACE USER WITH ID***
@app.put('/user/{id_param}',response_model = schemas.User_Response)
def replace_user(uri_id: int,user_body: schemas.UserPy, db:Session = Depends(get_db)):
    # Exception if not found
    up_usr = db.query(models.User).filter(models.User.id == uri_id)
    up_user = up_usr.first()
    
    if not up_user:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f"No corresponding User found in the DB:{uri_id}")
    
    up_usr.update(user_body.dict(), synchronize_session = False)
    db.commit()
    
    return up_user
            

#################################################################
####  PUT /blogpost/{id_param} ===> REPLACE BLOGPOST WITH ID  ###
#################################################################
            
#PUT /blogpost/{id_param}  ***REPLACE POST WITH ID***
@app.put('/blogpost/{uri_id}',response_model = schemas.BlogPost_Response)
def replace_blogspot(uri_id: int, blogpost_body: schemas.BlogpostPy, db: Session = Depends(get_db)):
    # Exception if not found    
    up_blog = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id)
    up_blogpost = up_blog.first()
    
    if not up_blogpost:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f"No corresponding Post found in the DB:{uri_id}")
    
    up_blog.update(blogpost_body.dict(), synchronize_session = False)
    
    db.commit()
    
    return up_blogpost
 